package ua.pp.switcher.config.resolver;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Configuration resolver for confgi.xml
 **/

public class ConfigResolver{
	
	private static final String CONFIG_TAG1 = "config";
	
	private static final String PATH_ATTRIBUTE = "path";
	
	private static final String ENV_TAG = "env";
	
	private static final String PARAM_TAG = "param";
	
	private static final String NAME_ATTRIBUTE = "name";
	
	private static final String VALUE_ATTRIBUTE = "value";
	
	
    public static  Map<String, String> getParams(File f, String fPath){
    	
		Map<String, String> params = new HashMap<String, String>();

		Parameters pars = new Parameters();
		FileBasedConfigurationBuilder<XMLConfiguration> builder = new FileBasedConfigurationBuilder<XMLConfiguration>(
				XMLConfiguration.class).configure(pars.xml().setFile(f));
		
		XMLConfiguration config;
		NodeList configsNList;		
		
		try {

			config = builder.getConfiguration();
			configsNList= config.getDocument().getElementsByTagName(CONFIG_TAG);
		
			Node node;
			String env = null;
			String path = null;
			
			for (int i = 0; i < configsNList.getLength(); i++) {

				node = configsNList.item(i);
				path = node.getAttributes().getNamedItem(PATH_ATTRIBUTE).getNodeValue();
				
				if (fPath.equals(path) || fPath.equals(path + "/")) {

					env = node.getAttributes().getNamedItem(ENV_TAG).getNodeValue();
					break;
				}
			}
			
			System.out.println("==============");
			System.out.println("Trying to get config by: " + "[" + env + "]"
					+ "[" + path + "]");
			
			NodeList envNList = config.getDocument().getElementsByTagName(ENV_TAG);
			
			String name = null;
			String value = null;
			
			NodeList paramsNList;
			
			for(int i = 0; i < envNList.getLength(); i++){
				
				name = envNList.item(i).getAttributes().getNamedItem(NAME_ATTRIBUTE).getNodeValue();
				
				if(name.equals(env)){
					
					paramsNList = envNList.item(i).getChildNodes();
					
					for(int j = 0; j < paramsNList.getLength(); j++ ){
						
						if(paramsNList.item(j) != null && paramsNList.item(j).getNodeName().equals(PARAM_TAG)){
							
							name = paramsNList.item(j).getAttributes().getNamedItem(NAME_ATTRIBUTE).getNodeValue();
							value = paramsNList.item(j).getAttributes().getNamedItem(VALUE_ATTRIBUTE).getNodeValue();						
							params.put(name, value);
							
						}
					}
				}
			}

		} catch (ConfigurationException e) {

			e.printStackTrace();
		}
		
		return params;
    }
}
